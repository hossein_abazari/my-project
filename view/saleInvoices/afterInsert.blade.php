@component('template.pages.prints')
    @slot('title')
        فاکتور فروش
        @endslot
    @slot('numberFactor') {{ $factor->factor_code }} @endslot
    @slot('customerName') {{ $factor->people->person_name }} @endslot
    @slot('mobile') {{ $factor->people->mobile }} @endslot
    @slot('address') {{ $factor->people->address }} @endslot
    @slot('date') {{ $factor->pre_factor_date }} @endslot
    @slot('factor')  @endslot
    @slot('discount') {{ number_format($factor->discount) }}@endslot
    @slot('taxTotal') {{ number_format($factorDetails->sum('tax')) }}@endslot
    @slot('sumTotal') {{ number_format($factor->amount) }}@endslot
    @slot('totalAmount') {{ number_format($totalAmount) }}@endslot
    @slot('detail') {{ $factor->detail }}@endslot
    @slot('thisSlug') {{ $factor->slug }}@endslot
    @slot('nameRegister') {{ $factor->users->people->person_name  }}@endslot
    @slot('returnRoute') {{ route('sale.invoices.index') }} @endslot

    @slot('contentDetail')
        @foreach($factorDetails as $key=>$factorDetail)
        <tr class="text-right">
            <td>{{$key+1}}</td>
            <td>{{ $factorDetail->products->product_name }}</td>
            <td>{{ number_format($factorDetail->amount) }}</td>
            <td>{{ $factorDetail->count }}</td>
            <td>{{ $factorDetail->products->unitGoods->unit_good_name }}</td>
            <td>{{ number_format($factorDetail->amount * $factorDetail->count) }}</td>
            <td>{{ number_format($factorDetail->tax) }}</td>
            <td>{{ number_format(($factorDetail->amount * $factorDetail->count) + $factorDetail->tax) }}</td>
        </tr>
        @endforeach
    @endslot
    @endcomponent
