

<div class="row-title table-responsive border-green">
    <div class="row">
        <div class="col-md-4">
            <b>{{ trans('validation.custom.crm.invoiceNumber') }}</b>: {{$factor->factor_code}}
        </div>

        <div class="col-md-4">
            <b>{{ trans('validation.custom.crm.customerName') }}</b>: {{$factor->people->person_name}}
        </div>

        <div class="col-md-4">
            <b>{{ trans('validation.custom.crm.saleInvoiceDate') }}</b>: {{$factor->factor_date}}
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <b>{{ trans('validation.custom.amount') }}</b>: {{number_format($factor->amount + $factor->discount - $factor->factorDetail()->get()->sum('tax') )}} {{ trans('validation.custom.rial') }}
        </div>

        <div class="col-md-4">
            <b>{{ trans('validation.custom.discount') }}</b>: {{number_format($factor->discount)}} {{ trans('validation.custom.rial') }}
        </div>

        <div class="col-md-4">
            <b>{{ trans('validation.custom.crm.tax') }}</b>: {{number_format($factor->factorDetail()->get()->sum('tax'))}} {{ trans('validation.custom.rial') }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <b>{{ trans('validation.custom.totalAmount') }}</b>: {{number_format($factor->amount)}} {{ trans('validation.custom.rial') }}
        </div>

        <div class="col-md-4">
            <b>{{ trans('validation.custom.crm.amountPaid') }}</b>: <span class="text-danger">{{number_format($factor->payment_amount)}} {{ trans('validation.custom.rial') }}</span>
        </div>

        <div class="col-md-4">
            <b>{{ trans('validation.custom.crm.remainingAmount') }}</b>: <span class="text-danger">{{number_format($factor->amount - $factor->payment_amount)}} {{ trans('validation.custom.rial') }}</span>
        </div>
    </div>
</div>
<hr>
@component('template.pages.tables')
    @slot('render') <div class="render-tables" id="{{$factor->slug}}">{{ $factorDetails->render() }} </div> @endslot
    @slot('class') border-green @endslot
    @slot('content')
        <thead>
        <tr>
            <th>{{ trans('validation.custom.row') }}</th>
            <th>{{ trans('validation.custom.crm.productName') }}</th>
            <th>{{ trans('validation.custom.number') }}</th>
            <th>{{ trans('validation.custom.amount') }}({{ trans('validation.custom.rial') }})</th>
            <th>{{ trans('validation.custom.crm.tax') }}({{ trans('validation.custom.rial') }})</th>
            <th>{{ trans('validation.custom.totalAmount') }}({{ trans('validation.custom.rial') }})</th>
        </tr>
        </thead>
        <tbody id="tables_all">
        @unless($factorDetails->isEmpty())
            @foreach($factorDetails as $factorDetail)
                <tr>
                    <td>{{(($factorDetails->currentPage()-1) * $factorDetails->perPage()) + $loop->iteration}}</td>
                    <td>{{$factorDetail->products->product_name}}</td>
                    <td>{{$factorDetail->count}}</td>
                    <td>{{number_format($factorDetail->amount)}}</td>
                    <td>{{number_format($factorDetail->tax)}}</td>
                    <td>{{number_format( ($factorDetail->amount * $factorDetail->count) + $factorDetail->tax)}}</td>

                </tr>
            @endforeach
        @else
            <tr>
                <th colspan="4"><p class="text-center">{{ trans('validation.custom.notFound') }}</p></th>
            </tr>
        @endunless
        </tbody>
        @endslot
    @endcomponent


<script>
    $(window).on('hashchange', function () {
        if (window.location.hash) {
            var page = window.location.hash.replace('#', '');
            if (page == Number.NaN || page <= 0) {
                return false;
            } else {
                getData(page);
            }
        }
    });
    $(document).ready(function () {
        $(document).on('click', '.render-tables .pagination a', function (event) {
            event.preventDefault();
            $('li').removeClass('active');
            $(this).parent('li').addClass('active');

            var myurl = $(this).attr('href');
            var page = $(this).attr('href').split('page=')[1];

            var id_ajax = event.target.parentNode.parentNode.parentNode.id;
            $.ajax(
                {
                    url: `{{route('sale.invoices.index')}}` + "/" + id_ajax + '?page=' + page,
                    type: "get",
                    datatype: "html"
                }).done(function (data) {
                $("#modal_pre_factor_content").empty().html(data);
                location.hash = page;
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                alert('No response from server');
            });

        });
    });

</script>
