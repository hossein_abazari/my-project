@extends('business.crm.master')

@section('title')
    {{ trans('validation.custom.crm.saleInvoice') }}
@endsection

@section('content')
    @include('partial.flash')
    <div class="row">
        <!-- BEGIN BREADCRUMB -->

        @component('template.pages.title')
            @slot('title1') {{ trans('validation.custom.dashboard') }} @endslot
            @slot('title2') {{ trans('validation.custom.pages') }} @endslot
            @slot('title3') {{ trans('validation.custom.crm.saleInvoice') }} @endslot
            @slot('button')
                    @can('businessCrmSaleInvoiceInsert')

                        <div class="btn-group">

                            <button  class="btn pull-right hidden-sm-down bg-orange-red btn-circle dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-plus-square-o"></i> {{ trans('validation.custom.created') }}</button>


                            <ul class="dropdown-menu" style="margin: 0px -38px 0px 0px;">
                                <li>
                                    <a href="{{ route('sale.invoices.create') }}">{{ trans('validation.custom.crm.saleInvoice') }}</a>
                                </li>
                                <li>
                                    <a href="{{ route('sale.invoices.back') }}"> {{ trans('validation.custom.crm.saleBack') }} </a>
                                </li>

                            </ul>
                        </div>
                        @endcan
            @endslot

        @endcomponent

        <div class="col-md-12">
            <div class="portlet box border panel-content">
                <div class="portlet-heading">
                    <div class="portlet-title">
                        <h3 class="title">
                            <i class="fa fa-pencil"></i>
                                {{ trans('validation.custom.crm.saleInvoice') }}
                        </h3>
                    </div><!-- /.portlet-title -->
                    <!-- /.buttons-box -->
                </div>
            @component('template.pages.search')
                    @slot('route_search') {{ route('sale.invoices.index') }} @endslot
                    @slot('content')
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <input class="form-control" name="sale_invoice_code_search" onkeydown="notSubmit();" placeholder="{{ trans('validation.custom.crm.code') }}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input class="form-control typeaheadPersonName" name="customer_name_search" onkeydown="notSubmit();" placeholder="{{ trans('validation.custom.crm.customerName') }}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input class="form-control" id="from_date" name="from_date" onkeydown="notSubmit();" placeholder="{{ trans('validation.custom.crm.fromDate') }}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input class="form-control" id="to_date" name="to_date" onkeydown="notSubmit();" placeholder="{{ trans('validation.custom.crm.toDate') }}">
                                </div>
                            </div>
                            <div class="col-md-1">
                            </div>
                        {{-- row--}}
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input class="form-control" name="from_amount" onkeydown="notSubmit();" placeholder="{{ trans('validation.custom.crm.fromAmount') }}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input class="form-control" name="from_amount" onkeydown="notSubmit();" placeholder="{{ trans('validation.custom.crm.fromAmount') }}">
                                </div>
                            </div>

                        @endslot
                    @endcomponent

                @component('template.pages.tables')
                    @slot('render') {{ $factors->render() }} @endslot
                    @slot('content')
                        <thead class="thead-count">
                        <tr>
                            <th>{{ trans('validation.custom.row') }}</th>
                            <th>{{ trans('validation.custom.crm.saleInvoiceCode') }}</th>
                            <th>{{ trans('validation.custom.crm.customerName') }}</th>
                            <th>{{ trans('validation.custom.crm.saleInvoiceDate') }}</th>
                            <th>{{ trans('validation.custom.amount') }} ({{ trans('validation.custom.rial') }})</th>
                            <th>{{ trans('validation.custom.discount') }} ({{ trans('validation.custom.rial') }})</th>
                            <th>{{ trans('validation.custom.crm.tax') }} ({{ trans('validation.custom.rial') }})</th>
                            <th>{{ trans('validation.custom.totalAmount') }} ({{ trans('validation.custom.rial') }})</th>
                            <th>{{ trans('validation.custom.crm.typeFactor') }}</th>
                            <th>{{ trans('validation.custom.status') }}</th>
                            <th style="width: 160px;">{{ trans('validation.custom.operation') }}</th>
                        </tr>
                        </thead>
                        <tbody id="tables_all">
                        @can('businessCrmSaleInvoiceView')
                            @unless($factors->isEmpty())
                                @foreach($factors as $factor)
                                    <tr>
                                        <td>{{(($factors->currentPage()-1) * $factors->perPage()) + $loop->iteration}}</td>
                                        <td>{{$factor->factor_code}}</td>

                                        <td>{{$factor->people->person_name}}</td>
                                        <td>{{$factor->factor_date}}</td>
                                        <td>{{number_format($factor->amount + $factor->discount - $factor->factorDetail()->get()->sum('tax') )}}</td>
                                        <td>{{number_format($factor->discount)}}</td>
                                        <td>{{number_format($factor->factorDetail()->get()->sum('tax'))}}</td>
                                        <td>{{number_format($factor->amount)}}</td>
                                        <td>
                                            @if ($factor->type == 'saleInvoice')
                                                {{ trans('validation.custom.crm.saleInvoice') }}
                                            @elseif ($factor->type == 'saleBack')
                                                {{ trans('validation.custom.crm.saleBack') }}
                                            @endif
                                        </td>
                                        <td>
                                            @if ($factor->payment == 1)
                                                <span class="badge badge-success">
                                                    {{ trans('validation.custom.crm.paid') }}
                                                </span>
                                                @elseif($factor->payment_amount > 0)
                                                <span class="badge badge-info">
                                                    {{ trans('validation.custom.crm.prePayment') }}
                                                </span>
                                            @elseif($factor->payment_amount == 0)
                                                <span class="badge badge-warning">
                                                    {{ trans('validation.custom.crm.noPayment') }}
                                                </span>
                                            @endif
                                        </td>
                                        <td>
                                            @can('businessCrmSaleInvoiceDelete_dont_view')
                                            <button class="btn btn-link btn-text-danger btn-delete " data-name="{{$factor->name}}" data-id="{{$factor->slug}}" data-toggle="modal" data-target="#modal-delete" ><i class="ti-trash"></i></button>
                                            @endcan
                                          @can('businessCrmSaleInvoiceEdit')
                                                <a class="btn btn-link btn-text-primary btn_edit" href="{{route('sale.invoices.edit',['slug'=>$factor->slug])}}"><i class="ti-pencil-alt"></i></a>

                                            @endcan

                                            @can('businessCrmSaleInvoiceShow')
                                                <button class="btn btn-link text-success btn_details" data-slug="{{$factor->slug}}" data-toggle="modal" data-target="#modal_detail" ><i class="fa fa-info"></i></button>
                                            @endcan
                                                <a class="btn btn-link text-navy-blue btn_print" href="{{route('sale.invoices.after.insert',['slug'=>$factor->slug])}}"><i class="fa fa-print"></i></a>


                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <th class="title-th" colspan=""><p class="text-center">{{ trans('validation.custom.notFound') }}</p></th>
                                </tr>
                            @endunless
                        @else
                            <tr>
                                <th class="title-th" colspan=""><p class="text-center">{{ trans('validation.custom.notPermission') }}</p></th>
                            </tr>
                        @endcan
                        </tbody>
                        @endslot
                    @endcomponent

            </div><!-- /.portlet -->
        </div><!-- /.col-md-12 -->
    </div><!-- /.row -->


    {{--delete modals--}}
    @can('businessCrmSaleInvoiceShow')
        @component('template.pages.delete_modals')
        @endcomponent
    @endcan

    @can('businessCrmSaleInvoiceShow')
        @component('template.pages.modal_empty')
            @slot('title') {{ trans('validation.custom.crm.details') }} {{ trans('validation.custom.crm.saleInvoice') }} @endslot
            @slot('idModal') modal_detail @endslot
            @slot('sizeModal') modal-lg @endslot
            @slot('colorHeader') header-modal-green @endslot
            @slot('colorFooter') footer-modal-green-white @endslot

            @slot('content')
                <div id="modal_pre_factor_content">

                </div>


            @endslot
        @endcomponent
    @endcan


@endsection
@section('css')
    <link href="{{asset('css/dist/tableBlack.css')}}" rel="stylesheet">

@endsection

@section('script')

    <script>
        kamaDatepicker('from_date', customOptions);
        kamaDatepicker('to_date', customOptions);
        function countTh() {
            var tds = $('.thead-count').children('tr').children('th').length;
            $('.title-th').attr('colspan',tds);
        }
        countTh();
        $(document).on('click','.btn_details',function () {
            var slug = $(this).data('slug');
            $.get(`{{route('sale.invoices.index')}}/`+slug,function (data) {
                $('#modal_pre_factor_content').html(data)
            });
        });
        $(document).on('focus','.price',function () {
            $('.rial-text').css('color','white')
        });

        $(document).on('focusout','.price',function () {
            $('.rial-text').css('color','#aaaaaa')
        });

        // $('.selectpicker').selectpicker();
            var path = "{{ route('customers.autocomplete') }}";
            $('input.typeaheadPersonName').typeahead({

                source:  function (query, process) {
                    if (query.length > 3) {
                        return $.get(path, {query: query,name:'person_name'}, function (data) {
                            return process(data);
                        });
                    }
                }
            });




        $(document).on('click','.btn-delete',function (e) {
            var id = $(this).data('id');
            var name = $(this).data('name');
            $('#form_delete').attr('action',`{{route('sale.invoices.index')}}/`+id);
            $("#title_name").html(name)
        });
        $(document).on('click','.btn_edit',function (e) {
            var id = $(this).data('id');
            $.get(`{{route('sale.invoices.index')}}/`+id+'/edit',function (data) {
                $('.content-modal-edit').html(data)
            });
        });
        //search ajax
        $(document).on('click','#btn_search',function (e) {
            $('#tables_all').html("<tr>" +
                "<th colspan='11' class='text-center title-th'><i class='fa fa-circle-o-notch fa-spin'></i></th>" +
                "</tr>");
            var method = $($(this)).closest('form').attr('method');
            var url = $($(this)).closest('form').attr('action');
            var data = $($(this)).closest('form').serialize();
            $.ajax({
                type: method,
                url: url,
                data: data,
                success: function (response) {
                    $('#tables_all').html(response);
                }
            })
        });

        $(document).on('click','#add_btn',function (e) {
            var elementClick = $(this);
            ajaxJquery(`{{route('sale.invoices.index')}}`,elementClick,"<i class='ti-save-alt'></i> {{trans('validation.custom.insert')}}");
        })
        $(document).on('click','.btn-edit-user',function (e) {
            var elementClick = $(this);
            ajaxJquery(`{{route('sale.invoices.index')}}`,elementClick,"<i class='fa fa-save'></i>",1);
        });
        $(document).on('click','#update_btn',function (e) {
            var elementClick = $(this);
            ajaxJquery(`{{route('sale.invoices.index')}}`,elementClick,"<i class='fa fa-pencil'></i> {{trans('validation.custom.edit_title')}}");
        })


    </script>

@endsection

