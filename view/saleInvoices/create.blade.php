@extends('business.crm.master')

@section('title')
    @if(Request::is('business/crm/sale/invoice/back'))
        {{ trans('validation.custom.crm.saleBack') }}
    @else
        {{ trans('validation.custom.crm.saleInvoice') }}
    @endif
@endsection

@section('content')
    @include('partial.flash')

    <div class="row">
        <!-- BEGIN BREADCRUMB -->

        @component('template.pages.title')
            @slot('title1') {{ trans('validation.custom.dashboard') }} @endslot
            @slot('title2') {{ trans('validation.custom.business') }} @endslot
            @slot('title3')    @if(Request::is('business/crm/sale/invoice/back'))
                    {{ trans('validation.custom.crm.saleBack') }}
                @else
                    {{ trans('validation.custom.crm.saleInvoice') }}
                @endif @endslot
            @slot('button') <a class="btn pull-right hidden-sm-down btn-info btn-circle" href="{{route('sale.invoices.index')}}"><i class="fa fa-arrow-left"></i> {{ trans('validation.custom.back') }}</a> @endslot
    @endcomponent

        <div class="col-md-12">

            <div class="portlet box border panel-content">
                <div class="portlet-heading header-page-orange">
                    <div class="portlet-title">
                        <h3 class="title">
                            <i class="ti-save"></i>
                            {{ trans('validation.custom.registerItem') }}
                        </h3>
                    </div><!-- /.portlet-title -->
                    <!-- /.buttons-box -->
                </div>

                <form action="{{route('sale.invoices.store')}}" method="POST" autocomplete="off" >
                    @csrf
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label>{{ trans('validation.custom.crm.invoiceNumber') }}</label>
                                <input class="form-control" name="factor_code" type="text" value="{{ $autoCode }}" placeholder="{{ trans('validation.custom.crm.saleInvoiceCode') }}">
                              @if(Request::is('business/crm/sale/invoice/back'))
                                <input class="form-control" name="sale_back" type="hidden" value="seleBack">
                                  @endif
                            </div>
                        </div>
                        <div class="col-md-2 ">
                            <div class="form-group">
                                <label>{{ trans('validation.custom.crm.customerName') }}</label>
                                <select  class="form-control  selectpicker select-customer sel-ajax ajax-customer" data-live-search="true" name="customer_id" title="{{ trans('validation.custom.crm.customerName') }}">
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="depot_name">{{ trans('validation.custom.crm.saleInvoiceDate') }} </label>
                                <input class="form-control" id="factor_date" name="factor_date" type="text" value="{{\Morilog\Jalali\Jalalian::now()->format('Y/m/d')}}"  placeholder="{{ trans('validation.custom.crm.saleInvoiceDate') }}">
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="depot_name">{{ trans('validation.custom.discount') }} </label>
                                <input class="form-control" name="discount" type="text" value="0"  placeholder="{{ trans('validation.custom.discount') }}" onkeyup="javascript:this.value=itpro(this.value);">
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="depot_name">{{ trans('validation.custom.crm.description') }} </label>
                                <input class="form-control" name="detail" type="text" value=""  placeholder="{{ trans('validation.custom.crm.description') }}">
                            </div>
                        </div>
                        <div class="col-md-1"></div>

                    </div>

                    <div class="row">
                        <div class="form-group">
                               @component('template.pages.row_factor')
                                @slot('routeProduct') {{ route('general.select.depot.product') }} @endslot
                                  @if( \App\Http\Controllers\business\crm\ToolController::tax()['tax'] == true) @slot('tax') @endslot @endif
                            @endcomponent
                        </div>
                    </div>

                </form>
            </div>
        </div>

    </div>

@endsection



@section('script')

    <script>
        $(document).ready(function () {
            kamaDatepicker('factor_date', customOptions);

            $(document).on('click', ".dropdown-item", function () {
                const selected_id = $('.ajax-customer option:selected').val();
                getUserAjax(selected_id);
            });
            var optionCustomers = {
                ajax: {
                    url: "{{ route('general.select.customer') }}",
                    type: "GET",
                    dataType: "json",
                },
                locale: {
                    emptyTitle: "Select and Begin Typing"
                },
                preprocessData: function (data) {
                    var i,
                        l = data.length,
                        array = [];
                    if (l) {
                        for (i = 0; i < l; i++) {
                            array.push(
                                $.extend(true, data[i], {
                                    text:
                                        "نام:" +
                                        data[i].person_name +
                                        "--تلفن همراه:" +
                                        data[i].mobile ,
                                    value: data[i].id,
                                })
                            );
                        }
                    }
                    return array;
                }
            };

            $(".select-customer").selectpicker()
                .filter(".sel-ajax")
                .ajaxSelectPicker(optionCustomers);
        });

    </script>

    @endsection
