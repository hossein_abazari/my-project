@extends('business.crm.master')

@section('title')
    {{ trans('validation.custom.crm.saleInvoice') }}
@endsection

@section('content')
    @include('partial.flash')

    <div class="row">
        <!-- BEGIN BREADCRUMB -->

        @component('template.pages.title')
            @slot('title1') {{ trans('validation.custom.dashboard') }} @endslot
            @slot('title2') {{ trans('validation.custom.business') }} @endslot
            @slot('title3') {{ trans('validation.custom.edit_title') }} {{ trans('validation.custom.crm.saleInvoice') }} @endslot
            @slot('button') <a class="btn pull-right hidden-sm-down btn-info btn-circle" href="{{route('sale.invoices.index')}}"><i class="fa fa-arrow-left"></i> {{ trans('validation.custom.back') }}</a> @endslot
        @endcomponent

        <div class="col-md-12">

            <div class="portlet box border panel-content">
                <div class="portlet-heading header-page-orange">
                    <div class="portlet-title">
                        <h3 class="title">
                            <i class="ti-pencil"></i>
                            {{ trans('validation.custom.edit_title') }} {{ $factor->factor_code }}
                        </h3>
                    </div><!-- /.portlet-title -->
                    <!-- /.buttons-box -->
                </div>

                <form action="{{route('sale.invoices.update',['id'=>$factor->slug])}}" method="POST" autocomplete="off" >
                    @method('PATCH')
                    @csrf
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-sm-1">
                            <div class="form-group">
                                <label>{{ trans('validation.custom.crm.saleInvoiceCode') }}</label>
                                <input class="form-control" type="text" readonly value="{{ $factor->factor_code }}" placeholder="{{ trans('validation.custom.crm.saleInvoiceCode') }}">
                            </div>
                        </div>
                      @if ($factor->people->mobile==null)
                            @php
                                $mobile = "ندارد";
                                @endphp
                          @else

                            @php
                                $mobile = $factor->people->mobile;
                            @endphp

                      @endif
                        <div class="col-md-2 ">
                            <div class="form-group">
                                <label>{{ trans('validation.custom.crm.customerName') }}</label>
                                <select  class="form-control  selectpicker select-customer sel-ajax ajax-customer" data-live-search="true" name="customer_id" title="{{ trans('validation.custom.crm.customerName') }}">
                                <option selected value="{{ $factor->person_id }}">نام:{{$factor->people->person_name}}--تلفن همراه:{{$mobile}}</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="depot_name">{{ trans('validation.custom.crm.saleInvoiceDate') }} </label>
                                <input class="form-control" name="factor_date" type="text" value="{{$factor->factor_date}}"  placeholder="{{ trans('validation.custom.crm.saleInvoiceDate') }}">
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="depot_name">{{ trans('validation.custom.discount') }} </label>
                                <input class="form-control" name="discount" type="text" value="{{number_format($factor->discount)}}"  placeholder="{{ trans('validation.custom.discount') }}" onkeyup="javascript:this.value=itpro(this.value);">
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="depot_name">{{ trans('validation.custom.crm.description') }} </label>
                                <input class="form-control" name="detail" type="text" value="{{$factor->detail}}"  placeholder="{{ trans('validation.custom.crm.description') }}">
                            </div>
                        </div>
                        <div class="col-md-1"></div>

                    </div>

                    <div class="row">
                        <div class="form-group">
                            @component('template.pages.row_factor')
                                @slot('routeProduct') {{ route('general.select.depot.product') }} @endslot
                                    @slot('buttonInsert')
                                        {{ trans('validation.custom.edit_title') }}
                                    @endslot
                                @slot('routeRefresh') {{ route('sale.invoices.index') }} @endslot
                                @if( \App\Http\Controllers\business\crm\ToolController::tax()['tax'] == true) @slot('tax') @endslot @endif
                                    @slot('edit_row')
                                        @foreach($factorDetails as $factorDetail)
                                            <div class="row row-margin-0" style="border: 1px solid #eee">
                                                <div class="col-md-3 col-row border-blue-top">
                                                    <div class="form-group">
                                                        <select  class="form-control form-control-row selectpicker select-product with-ajax ajax-select" data-live-search="true" name="product_id[]" title="{{ trans('validation.custom.crm.productTitle') }}">
                                                            <option value="{{ $factorDetail->products->id }}">نام:{{$factorDetail->products->product_name}}
                                                                --گروه:{{$factorDetail->products->groupProducts->group_product_name}}--بارکد:{{$factorDetail->products->barcode}}--موجودی:{{$factorDetail->products->depotDetail()->get()->sum('count')}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-row">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control form-control-row count" value="{{ $factorDetail->count }}" name="count[]">
                                                        <div class="amount-text">{{ trans('validation.custom.number') }}:</div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-row">
                                                    <div class="form-group">
                                                        <input class="form-control form-control-row amount" name="amount[]" value="{{ number_format($factorDetail->amount) }}" onkeyup="javascript:this.value=itpro(this.value);" >
                                                        <div class="rial-text rial-row">{{ trans('validation.custom.rial') }}</div>
                                                        <div class="amount-text">{{ trans('validation.custom.amount') }}:</div>
                                                    </div>
                                                </div>

                                                <div class="col-md-2 col-row">
                                                    <div class="form-group">
                                                        <input class="form-control form-control-row tax" name="tax[]" value="{{ number_format($factorDetail->tax) }}" onkeyup="javascript:this.value=itpro(this.value);" >
                                                        <div class="rial-text rial-row">{{ trans('validation.custom.rial') }}</div>
                                                        <div class="amount-text">{{ trans('validation.custom.crm.tax') }}:</div>
                                                    </div>
                                                </div>

                                                <div class="col-md-2 col-row">
                                                    <div class="form-group">
                                                        <input class="form-control form-control-row totalAmount" readonly value="{{ number_format($factorDetail->amount * $factorDetail->count) }}" onkeyup="javascript:this.value=itpro(this.value);" >
                                                        <div class="rial-text rial-row">{{ trans('validation.custom.rial') }}</div>
                                                        <div class="amount-text">{{ trans('validation.custom.totalAmount') }}:</div>
                                                    </div>

                                                </div>
                                                <div class="col-md-1 col-row del-col-row">
                                                    <button type="button" class="btn btn-link text-danger delete-row"><i class="fa fa-trash-o"></i></button>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endslot
                            @endcomponent
                        </div>
                    </div>

                </form>
            </div>
        </div>

    </div>

@endsection



@section('script')

    <script>
        $(document).ready(function () {
            $(document).on('click', ".dropdown-item", function () {
                const selected_id = $('.ajax-customer option:selected').val();
                getUserAjax(selected_id);
            });
            var optionCustomers = {
                ajax: {
                    url: "{{ route('general.select.customer') }}",
                    type: "GET",
                    dataType: "json",
                },
                locale: {
                    emptyTitle: "Select and Begin Typing"
                },
                preprocessData: function (data) {
                    var i,
                        l = data.length,
                        array = [];
                    if (l) {
                        for (i = 0; i < l; i++) {
                            array.push(
                                $.extend(true, data[i], {
                                    text:
                                        "نام:" +
                                        data[i].person_name +
                                        "--تلفن همراه:" +
                                        data[i].mobile ,
                                    value: data[i].id,
                                })
                            );
                        }
                    }
                    return array;
                }
            };

            $(".select-customer").selectpicker()
                .filter(".sel-ajax")
                .ajaxSelectPicker(optionCustomers);
        });

    </script>

@endsection
