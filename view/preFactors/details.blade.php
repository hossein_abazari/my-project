@component('template.pages.tables')
    @slot('render') <div class="render-tables" id="{{$preFactor->slug}}">{{ $preFactorDetails->render() }} </div> @endslot
    @slot('class') border-green @endslot
    @slot('content')
        <thead>
        <tr>
            <th>{{ trans('validation.custom.row') }}</th>
            <th>{{ trans('validation.custom.crm.productName') }}</th>
            <th>{{ trans('validation.custom.number') }}</th>
            <th>{{ trans('validation.custom.amount') }}</th>
            <th>{{ trans('validation.custom.crm.tax') }}</th>
            <th>{{ trans('validation.custom.totalAmount') }}</th>
        </tr>
        </thead>
        <tbody id="tables_all">
        @unless($preFactorDetails->isEmpty())
            @foreach($preFactorDetails as $preFactorDetail)
                <tr>
                    <td>{{(($preFactorDetails->currentPage()-1) * $preFactorDetails->perPage()) + $loop->iteration}}</td>
                    <td>{{$preFactorDetail->products->product_name}}</td>
                    <td>{{$preFactorDetail->count}}</td>
                    <td>{{number_format($preFactorDetail->amount)}}</td>
                    <td>{{number_format($preFactorDetail->tax)}}</td>
                    <td>{{number_format( ($preFactorDetail->amount * $preFactorDetail->count) + $preFactorDetail->tax)}}</td>

                </tr>
            @endforeach
        @else
            <tr>
                <th colspan="4"><p class="text-center">{{ trans('validation.custom.notFound') }}</p></th>
            </tr>
        @endunless
        </tbody>
        @endslot
    @endcomponent
    <br>
<a  class="btn btn-primary" href="{{ route('prefactors.convert',['slug'=>$preFactor->slug]) }}"  onclick="event.preventDefault();
                                                     document.getElementById('convert').submit();" >
    تبدیل به فاکتور
</a>
<form id="convert" action="{{ route('prefactors.convert',['slug'=>$preFactor->slug]) }}" method="POST" style="display: none;">
    @csrf
</form>

<script>
    $(window).on('hashchange', function () {
        if (window.location.hash) {
            var page = window.location.hash.replace('#', '');
            if (page == Number.NaN || page <= 0) {
                return false;
            } else {
                getData(page);
            }
        }
    });
    $(document).ready(function () {
        $(document).on('click', '.render-tables .pagination a', function (event) {
            event.preventDefault();
            $('li').removeClass('active');
            $(this).parent('li').addClass('active');

            var myurl = $(this).attr('href');
            var page = $(this).attr('href').split('page=')[1];

            var id_ajax = event.target.parentNode.parentNode.parentNode.id;
            $.ajax(
                {
                    url: `{{route('prefactors.index')}}` + "/" + id_ajax + '?page=' + page,
                    type: "get",
                    datatype: "html"
                }).done(function (data) {
                $("#modal_pre_factor_content").empty().html(data);
                location.hash = page;
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                alert('No response from server');
            });

        });
    });

</script>
