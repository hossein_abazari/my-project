@component('template.pages.prints')
    @slot('title')
        پیش فاکتور
        @endslot
    @slot('numberFactor') {{ $preFactor->pre_factor_code }} @endslot
    @slot('customerName') {{ $preFactor->people->person_name }} @endslot
    @slot('mobile') {{ $preFactor->people->mobile }} @endslot
    @slot('address') {{ $preFactor->people->address }} @endslot
    @slot('date') {{ $preFactor->pre_factor_date }} @endslot
    @slot('preFactor')  @endslot
    @slot('discount') {{ number_format($preFactor->discount) }}@endslot
    @slot('taxTotal') {{ number_format($preFactorDetails->sum('tax')) }}@endslot
    @slot('sumTotal') {{ number_format($preFactor->amount) }}@endslot
    @slot('totalAmount') {{ number_format($totalAmount) }}@endslot
    @slot('detail') {{ $preFactor->detail }}@endslot
    @slot('thisSlug') {{ $preFactor->slug }}@endslot
    @slot('nameRegister') {{ $preFactor->users->people->person_name  }}@endslot
    @slot('returnRoute') {{ route('prefactors.index') }} @endslot

    @slot('contentDetail')
        @foreach($preFactorDetails as $key=>$preFactorDetail)
        <tr class="text-right">
            <td>{{$key+1}}</td>
            <td>{{ $preFactorDetail->products->product_name }}</td>
            <td>{{ number_format($preFactorDetail->amount) }}</td>
            <td>{{ $preFactorDetail->count }}</td>
            <td>{{ $preFactorDetail->products->unitGoods->unit_good_name }}</td>
            <td>{{ number_format($preFactorDetail->amount * $preFactorDetail->count) }}</td>
            <td>{{ number_format($preFactorDetail->tax) }}</td>
            <td>{{ number_format(($preFactorDetail->amount * $preFactorDetail->count) + $preFactorDetail->tax) }}</td>
        </tr>
        @endforeach
    @endslot
    @endcomponent
