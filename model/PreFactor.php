<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class PreFactor extends Model
{

    protected $fillable=[
        'slug','pre_factor_code','detail','pre_factor_date','amount','discount','position','status'
    ];

    public function preFactorDetail()
    {
        return $this->hasMany(PreFactorDetail::class);
    }

    public function branches()
    {
        return $this->belongsTo(Branch::class,'branch_id');
    }

    public function users()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function people()
    {
        return $this->belongsTo(Person::class,'person_id');

    }


}
