<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Business extends Model
{
    use SoftDeletes;
    protected $fillable = ['slug','business_name','person_name','job_title','mobile','address','first_date','out_date','branch_count','status'];

    public function people()
    {
        return $this->hasMany(Person::class,'business_id');
    }

    public function logers()
    {
        return $this->hasMany(Loger::class,'business_id');
    }

    public function settings()
    {
        return $this->hasMany(Setting::class,'business_id');
    }

    public function modules()
    {
        return $this->belongsToMany(Module::class);
    }

    public function branch()
    {
        return $this->hasMany(Branch::class);
    }

    public function tariff()
    {
        return $this->hasMany(Tariff::class);
    }
    public function hasModule($module)
    {
        if (is_string($module)) {
            return $this->modules->contains('module_name', $module);
        }
        return !! $module->intersect($this->modules)->count();
    }
}
