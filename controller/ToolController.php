<?php

namespace App\Http\Controllers;

use App\AmountGeneral;
use App\BusinessAccount;
use App\BusinessModule;
use App\CountBranch;
use App\CounterDiscount;
use App\CounterPurchase;
use App\Depot;
use App\Discount;
use App\Message;
use App\Tariff;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Morilog\Jalali\Jalalian;

class ToolController extends Controller
{
    public static $amountMin = 50000;
    public static $ceiling_percent = 65;
    public static function businessHerself(){
        return \Auth::getUser()->people->branches->businesses;
    }
    public static function branchHerself(){
        return \Auth::getUser()->people->branches;
    }
    public static function peopleHerself(){
        return \Auth::getUser()->people;
    }


    public static function countBranchMax(){
        return "15";
    }
    public static function amountMinMessage()
    {
        return "10000";
    }
    public static function slug_random ($number=null){
        if ($number != null && ! is_string($number)){
            return substr(md5(uniqid(mt_rand(), true)), 0, $number);
        }
       return substr(md5(uniqid(mt_rand(), true)), 0, 8);
    }


    public static function discount_business($request,$deadline,$discount,$business,$discountZero)
    {


        if ($deadline->amount == 0){
            $discount_cent = 0;
        }
        else {
            if ($discount->isEmpty()) {
                $discount_cent = 0;
            } else {
                $discount_cent = (int)$discount->first()->discount_cent;
                $counterDiscount = new CounterDiscount();
                $counterDiscount->business_id = $business->id;
                $counterDiscount->discount_id = $discount->first()->id;
                $counterDiscount->save();
            }
            $discount_cent =(int)$discount_cent + (int)$discountZero->sum('discount_cent');
            if (!$discountZero->isEmpty()) {
                foreach ($discountZero as $item) {
                    $counterDiscountZero = new CounterDiscount();
                    $counterDiscountZero->business_id = $business->id;
                    $counterDiscountZero->discount_id = $item->id;
                    $counterDiscountZero->save();
                }
            }
        }
        $discount_amount = (int)$deadline->amount * (int)$discount_cent  / 100;
        $amount = (int)$deadline->amount - (int)$discount_amount;

        if ($request->percent_discount > 0){
            $shortage = (int)$amount * (int)$request->percent_discount / 100;
            $discount_cent = (int)$discount_cent + (int)$request->percent_discount;
        }
        elseif($request->amount_discount > 0){
            $shortage = str_replace(',','',$request->amount_discount);
        }
        else{
            $shortage = 0;
        }
        $amount = (int)$amount - (int)$shortage;
        return ['amount'=>$amount,'discount_cent'=>$discount_cent];
    }
    public static function discount_business_extended($request,$deadline,$business)
    {

        $counterPurchase = CounterPurchase::whereBusinessId($business->id)->get();
        $discountZero = Discount::whereNumber(0)->whereStatus(true)->where('expire_date','>',Carbon::now()->format('Y-m-d H:i:s'))->get();
        $discount = Discount::whereNumber($counterPurchase->count() + 1)->whereStatus(true)->where('expire_date','>',Carbon::now()->format('Y-m-d H:i:s'))->get();

        if (isset($request->percent_discount)) {
            if ($request->percent_discount > 0) {
                if (!$discount->isEmpty() && !$discountZero->isEmpty()) {
                    if ((int)$discount->first()->discount_cent + (int)$request->percent_discount + (int)$discountZero->sum('discount_cent') > ToolController::$ceiling_percent) { //
                        return json_encode(['status' => 200, 'alertErrors' => trans('validation.custom.errorsCeilingCent')]);
                    }
                }
                if (!$discount->isEmpty()) {
                    if ((int)$discount->first()->discount_cent + (int)$request->percent_discount > ToolController::$ceiling_percent) { //
                        return json_encode(['status' => 200, 'alertErrors' => trans('validation.custom.errorsCeilingCent')]);
                    }
                }
                if (!$discountZero->isEmpty()) {
                    if ((int)$discountZero->sum('discount_cent') + (int)$request->percent_discount > ToolController::$ceiling_percent) { //
                        return json_encode(['status' => 200, 'alertErrors' => trans('validation.custom.errorsCeilingCent')]);
                    }
                }
            }
        }

        if ($deadline->amount == 0){
            $discount_cent = 0;
        }
        else {
            if ($discount->isEmpty()) {
                $discount_cent = 0;
            } else {
                $discount_cent = (int)$discount->first()->discount_cent;
                $counterDiscount = new CounterDiscount();
                $counterDiscount->business_id = $business->id;
                $counterDiscount->discount_id = $discount->first()->id;
                $counterDiscount->save();
            }
            $discount_cent =(int)$discount_cent + (int)$discountZero->sum('discount_cent');
            if (!$discountZero->isEmpty()) {
                foreach ($discountZero as $item) {
                    $counterDiscountZero = new CounterDiscount();
                    $counterDiscountZero->business_id = $business->id;
                    $counterDiscountZero->discount_id = $item->id;
                    $counterDiscountZero->save();
                }
            }
        }
        $discount_amount = (int)$deadline->amount * (int)$discount_cent  / 100;
        $amount = (int)$deadline->amount - (int)$discount_amount;

        if ($request->percent_discount > 0){
            $shortage = (int)$amount * (int)$request->percent_discount / 100;
            $discount_cent = (int)$discount_cent + (int)$request->percent_discount;
        }
        elseif($request->amount_discount > 0){
            $shortage = str_replace(',','',$request->amount_discount);
        }
        else{
            $shortage = 0;
        }
        $amount = (int)$amount - (int)$shortage;
        return ['amount'=>$amount,'discount_cent'=>$discount_cent];
    }

    // convert time stamp to days
    public static function timeStampToDay($smallTime , $i , $bigTime) //for to foreach $i can before foreach
    {
        $out_date = \Carbon\Carbon::parse($bigTime)->getTimestamp();
        $numDays = abs($smallTime - $out_date)/60/60/24;
        if ($numDays > 1) {
            while ($i < $numDays) {
                $day[] = date('Y m d', strtotime("+{$i} day", time()));
                $i++;
            }
            return sizeof($day);

        }
        else{
            $day = 0;
            return $day;
        }

    }

    public static function merchantCodeManager()
    {
        $merchantCode = '73ba3492-3280-11e8-873b-005056a205be';
        return $merchantCode;
    }
    protected static function soap(){
        $soap = new \SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
        return $soap;
    }
    public static function verifyZarinpal($merchant_id , $amount,$description,$callbackURL,$mobile=null)
    {
        $client = ToolController::soap();
        $result = $client->PaymentRequest(
            [
                'MerchantID' => $merchant_id,
                'Amount' => substr($amount, 0, -1),
                'Description' => $description,
                'Mobile' => $mobile,
                'CallbackURL' => $callbackURL,
            ]
        );

        return $result;
    }

    public static function checkZarinpall($merchantID,$authority,$amount)
    {
        $client =  ToolController::soap();

        $result = $client->PaymentVerification(
            [
                'MerchantID' => $merchantID,
                'Authority' => $authority,
                'Amount' => substr($amount, 0, -1),
            ]
        );
        return $result;
    }

    public static function extendedDeadline ($request,$moduleIdKey,$businessId,$deadline,$amount,$discount_cent,$accountBusiness=null)
    {
        try {
            foreach ($moduleIdKey as $key => $value) {
                $tariffs = Tariff::whereModuleId($value)->whereBusinessId($businessId)->get();
                if ($tariffs->isEmpty()) {
                    $time = time() + (60 * 60 * 24 * $deadline->expire_date);
                    $out_date = Carbon::parse($time)->format('Y-m-d H:i:s');

                    $tariffSave = new Tariff();
                    $tariffSave->business_id = ToolController::slug_random();
                    $tariffSave->business_id = $businessId;
                    $tariffSave->module_id = $value;
                    $tariffSave->branch_count = $deadline->count_branch;
                    $tariffSave->first_date = Carbon::now()->format('Y-m-d H:i:s');
                    $tariffSave->out_date = $out_date;
                    $tariffSave->save();

                    $businessModule = new BusinessModule();
                    $businessModule->business_id = $businessId;
                    $businessModule->module_id = $value;
                    $businessModule->save();
                } else {
                    $timestamp = Carbon::parse($tariffs->first()->out_date)->getTimestamp();
                    if ($timestamp > time()) { //منقضی نیست
                        $time = $timestamp + (60 * 60 * 24 * $deadline->expire_date);
                        $out_date = Carbon::parse($time)->format('Y-m-d H:i:s');
                        $first_date = Carbon::parse($timestamp)->format('Y-m-d H:i:s');
                        $tariffs->first()->first_date = $first_date;
                        $tariffs->first()->out_date = $out_date;
                        $tariffs->first()->update();
                    } else {
                        $time = time() + (60 * 60 * 24 * $deadline->expire_date);
                        $out_date = Carbon::parse($time)->format('Y-m-d H:i:s');
                        $tariffs->first()->first_date = Carbon::now()->format('Y-m-d H:i:s');
                        $tariffs->first()->out_date = $out_date;
                        $tariffs->first()->update();
                    }
                }


                //            create BusinessAccount
                $slugBusinessAccount = ToolController::slug_random();
                $businessAccount = new BusinessAccount();
                $businessAccount->slug = $slugBusinessAccount;
                $businessAccount->business_id = $businessId;
                $businessAccount->deadline_id = $deadline->id;
                $businessAccount->module_id = $value;
                $businessAccount->amount = $amount;
                $businessAccount->discount_cent = $discount_cent;
                if ($accountBusiness ==null || $accountBusiness != 1) {
                    $businessAccount->discount_amount = str_replace(',', '', $request->amount_discount);
                }
                $businessAccount->save();

                if ($accountBusiness !=null && $accountBusiness == 1){
                    $slugBusinessAccount = ToolController::slug_random();
                    $businessAccount = new BusinessAccount();
                    $businessAccount->slug = $slugBusinessAccount;
                    $businessAccount->business_id = $businessId;
                    $businessAccount->deadline_id = $deadline->id;
                    $businessAccount->module_id = $value;
                    $businessAccount->amount = -$amount;
                    $businessAccount->discount_cent = $discount_cent;
                    $businessAccount->save();
                }
            }

            $amountGeneral = AmountGeneral::latest()->first();
            $message = new Message();
            $message->business_id = $businessId;
            $message->amount_general_id = $amountGeneral->id;
            $message->number = $deadline->count_sms * sizeof($moduleIdKey);
            $message->save();

            $counterPurchase = new CounterPurchase();
            $counterPurchase->business_id = $businessId;
            $counterPurchase->save();

            return true;
        }
        catch (\Exception $exception){
            return false;
        }
    }

    public static function messageSave($port,$amountGeneral)
    {
        try {
            $message = new Message();
            $message->business_id = $port->business_id;
            $message->amount_general_id = $amountGeneral['id'];
            $message->number = $port->module_array;
            $message->save();

            $businessAccount = new BusinessAccount();
            $businessAccount->slug = self::slug_random();
            $businessAccount->business_id = $port->business_id;
            $businessAccount->deadline_id = $port->deadline_id;
            $businessAccount->amount = $port->amount;
            $businessAccount->payment = 1;
            $businessAccount->save();

            $businessAccount1 = new BusinessAccount();
            $businessAccount1->slug = self::slug_random();
            $businessAccount1->business_id = $port->business_id;
            $businessAccount1->deadline_id = $port->deadline_id;
            $businessAccount1->amount = "-" . $port->amount;
            $businessAccount1->payment = 1;
            $businessAccount1->save();

            return true;
        }
        catch (\Exception $exception){
            return false;
        }
    }
    public static function branchSave($port,$amountGeneral)
    {
        try {
            $countBranch = new CountBranch();
            $countBranch->business_id = $port->business_id;
            $countBranch->amount_general_id = $amountGeneral['id'];
            $countBranch->number = $port->module_array;
            $countBranch->save();

            $businessAccount = new BusinessAccount();
            $businessAccount->slug = self::slug_random();
            $businessAccount->business_id = $port->business_id;
            $businessAccount->deadline_id = $port->deadline_id;
            $businessAccount->amount = $port->amount;
            $businessAccount->payment = 1;
            $businessAccount->save();

            $businessAccount1 = new BusinessAccount();
            $businessAccount1->slug = self::slug_random();
            $businessAccount1->business_id = $port->business_id;
            $businessAccount1->deadline_id = $port->deadline_id;
            $businessAccount1->amount = "-" . $port->amount;
            $businessAccount1->payment = 1;
            $businessAccount1->save();

            return true;
        }
        catch (\Exception $exception){
            return false;
        }
    }



}
