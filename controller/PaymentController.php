<?php

namespace App\Http\Controllers\business\crm;

use App\Check;
use App\CheckType;
use App\CrmPayment;
use App\Factor;
use App\Rules\ValidEmptySaleInvoice;
use App\Rules\ValidSlug;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentController extends Controller
{
    /**
     * @param Request $request
     * @return false|string
     */
    public function card(Request $request)
    {
        $request->validate([
            'slug'=>['required' ,new ValidEmptySaleInvoice()],
            'card_number_id'=>'required',
            'date_payment'=>'required',
            'amount'=>"required|regex:/^[0-9\.,]+$/|not_in:0",

        ]);
        $factor = Factor::whereSlug($request->slug)->first();
        $amount = str_replace(',','',$request->amount);
        if ($factor->amount - $factor->payment_amount >= $amount) {
            // create payment
            $crmPayment = new CrmPayment();
            $crmPayment->slug = \App\Http\Controllers\ToolController::slug_random(7);
            $crmPayment->factor_id = $factor->id;
            $crmPayment->amount = $amount;
            $crmPayment->person_id = $factor->person_id;
            $crmPayment->type = "card";
            $crmPayment->detail = $request->detail;
            $crmPayment->card_number_id = $request->card_number_id;
            $crmPayment->payment_date = $request->date_payment;
            $crmPayment->tracking_code = $request->tracking_code;
            if ($factor->type == "saleInvoice" || $factor->type == "purchaseBack") {
                $crmPayment->status = 0; // پرداختی مشتری
            }
            elseif ($factor->type == "saleBack" || $factor->type == "purchaseInvoice"){
                $crmPayment->status = 1; // پرداخت به مشتری
            }
            $crmPayment->save();

            $factor->payment_amount = $amount + $factor->payment_amount;
            if ($factor->amount - $factor->payment_amount <= 0) {
                $factor->payment = 1;
            }
            $factor->update();
            if (isset($request->detail)){
                $detail = $request->detail;
            }
            else{
                $detail = 'پرداخت کارت به کارت';
            }

            if ($factor->type == "saleInvoice") {
                ToolController::paymentAccountSave($factor, $detail, $amount);
                $url = route('sale.invoices.index');

            }
            elseif ($factor->type == "saleBack"){
                ToolController::paymentToCustomerAccountSave($factor, $detail, $amount);
                $url = route('sale.invoices.index');

            }
            elseif ($factor->type == "purchaseBack"){
                ToolController::paymentAccountSave($factor, $detail, $amount);
                $url = route('purchase.invoices.index');

            }
            elseif ($factor->type == "purchaseInvoice") {
                ToolController::paymentToCustomerAccountSave($factor, $detail, $amount);
                $url = route('purchase.invoices.index');

            }
        }
        else{
            flash(trans('validation.custom.validPayment'), 'danger');
            return json_encode(['status' => 100]);
        }

        flash(trans('validation.custom.success'), 'success');
        if ($factor->amount - $factor->payment_amount == 0){
            return json_encode(['status' => 120,'redirect'=>$url]);

        }
        return json_encode(['status' => 100]);

    }

    /**
     * @param Request $request
     * @return false|string
     */
    public function check(Request $request)
    {
        $request->validate([
            'slug'=>['required' ,new ValidEmptySaleInvoice()],
            'bank_id'=>'required',
            'amount'=>"required|regex:/^[0-9\.,]+$/|not_in:0",
            'check_number'=>'required|numeric|unique:checks,check_code',
            'check_serial'=>'required|numeric',
            'account_number'=>'required|numeric',

        ]);

        $factor = Factor::whereSlug($request->slug)->first();
        $amount = str_replace(',','',$request->amount);
        $checkType = CheckType::whereCode(1)->first();

        if ($factor->amount - $factor->payment_amount >= $amount) {

            // create check
            $check = new Check();
            $check->amount = $amount;
            $check->bank_id = $request->bank_id;
            $check->person_id = $factor->person_id;
            $check->slug = \App\Http\Controllers\ToolController::slug_random(7);
            $check->check_code = $request->check_number;
            $check->account_number = $request->account_number;
            $check->details = $request->detail;
            $check->branch_name = $request->branch_name;
            $check->branch_code = $request->branch_code;
            $check->check_serial = $request->check_serial;
            $check->check_type_id=$checkType->id;
            $check->checks_date = $request->date_check;
            $check->head_date = $request->head_date;
            $check->save();

            // create payment
            $crmPayment = new CrmPayment();
            $crmPayment->slug = \App\Http\Controllers\ToolController::slug_random(7);
            $crmPayment->check_id = $check->id;
            $crmPayment->factor_id = $factor->id;
            $crmPayment->amount = $amount;
            $crmPayment->person_id = $factor->person_id;
            $crmPayment->type = "check";
            $crmPayment->detail = $request->detail;
            $crmPayment->card_number_id = $request->card_number_id;
            $crmPayment->payment_date = $request->head_date;
            if ($factor->type == "saleInvoice" || $factor->type == "purchaseBack") {
                $crmPayment->status = 0; // پرداختی مشتری
            }
            elseif ($factor->type == "saleBack" || $factor->type == "purchaseInvoice"){
                $crmPayment->status = 1; // پرداخت به مشتری
            }
            $crmPayment->save();

            $factor->payment_amount = $amount + $factor->payment_amount;
            if ($factor->amount - $factor->payment_amount <= 0) {
                $factor->payment = 1;
            }
            $factor->update();
            if (isset($request->detail)){
                $detail = $request->detail;
            }
            else{
                $detail = 'پرداخت چک';
            }

            if ($factor->type == "saleInvoice") {
                ToolController::paymentAccountSave($factor, $detail, $amount);
                $url = route('sale.invoices.index');
            }
            elseif ($factor->type == "saleBack"){
                ToolController::paymentToCustomerAccountSave($factor, $detail, $amount);
                $url = route('sale.invoices.index');
            }
            elseif ($factor->type == "purchaseBack"){
                ToolController::paymentAccountSave($factor, $detail, $amount);
                $url = route('purchase.invoices.index');

            }
            elseif ($factor->type == "purchaseInvoice") {
                ToolController::paymentToCustomerAccountSave($factor, $detail, $amount);
                $url = route('purchase.invoices.index');
            }
        }
        else{
            flash(trans('validation.custom.validPayment'), 'danger');
            return json_encode(['status' => 100]);
        }

        flash(trans('validation.custom.success'), 'success');
        if ($factor->amount - $factor->payment_amount == 0){
            if ($factor->type == '')
            return json_encode(['status' => 120,'redirect'=>$url]);

        }
        return json_encode(['status' => 100]);
    }

    /**
     * @param Request $request
     * @return false|string
     */
    public function cash(Request $request)
    {
        $request->validate([
            'slug'=>['required' ,new ValidEmptySaleInvoice()],
            'date_payment'=>'required',
            'amount'=>"required|regex:/^[0-9\.,]+$/|not_in:0",

        ]);
        $factor = Factor::whereSlug($request->slug)->first();
        $amount = str_replace(',','',$request->amount);
        if ($factor->amount - $factor->payment_amount >= $amount) {
            // create payment
            $crmPayment = new CrmPayment();
            $crmPayment->slug = \App\Http\Controllers\ToolController::slug_random(7);
            $crmPayment->factor_id = $factor->id;
            $crmPayment->amount = $amount;
            $crmPayment->person_id = $factor->person_id;
            $crmPayment->type = "cash";
            $crmPayment->detail = $request->detail;
            $crmPayment->payment_date = $request->date_payment;
            if ($factor->type == "saleInvoice" || $factor->type == "purchaseBack") {
                $crmPayment->status = 0; // پرداختی مشتری
            }
            elseif ($factor->type == "saleBack" || $factor->type == "purchaseInvoice"){
                $crmPayment->status = 1; // پرداخت به مشتری
            }
            $crmPayment->save();

            $factor->payment_amount = $amount + $factor->payment_amount;
            if ($factor->amount - $factor->payment_amount <= 0) {
                $factor->payment = 1;
            }
            $factor->update();

            if (isset($request->detail)){
                $detail = $request->detail;
            }
            else{
                $detail = 'پرداخت نقدی';
            }

            if ($factor->type == "saleInvoice") {
                ToolController::paymentAccountSave($factor, $detail, $amount);
                $url = route('sale.invoices.index');

            }
            elseif ($factor->type == "saleBack"){
                ToolController::paymentToCustomerAccountSave($factor, $detail, $amount);
                $url = route('sale.invoices.index');

            }
            elseif ($factor->type == "purchaseBack"){
                ToolController::paymentAccountSave($factor, $detail, $amount);
                $url = route('purchase.invoices.index');

            }
            elseif ($factor->type == "purchaseInvoice") {
                ToolController::paymentToCustomerAccountSave($factor, $detail, $amount);
                $url = route('purchase.invoices.index');

            }
        }
        else{
            flash(trans('validation.custom.validPayment'), 'danger');
            return json_encode(['status' => 100]);
        }

        flash(trans('validation.custom.success'), 'success');

        if ($factor->amount - $factor->payment_amount == 0){
            return json_encode(['status' => 120,'redirect'=>$url]);

        }

        return json_encode(['status' => 100]);
    }
}
