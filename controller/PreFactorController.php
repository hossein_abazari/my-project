<?php

namespace App\Http\Controllers\business\crm;

use App\Codding;
use App\CrmAccount;
use App\Depot;
use App\DepotDetail;
use App\Factor;
use App\FactorDetail;
use App\PreFactor;
use App\PreFactorDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Morilog\Jalali\Jalalian;

class PreFactorController extends Controller
{


    /**
     * @param $request
     * @param $preFactors
     * @return string
     */
    public function search($request, $preFactors)
    {
        $tables ="";
        if (isset($request->pre_factor_code_search)){
            $preFactors = $preFactors
                ->where('pre_factor_code','like','%'.$request->pre_factor_code_search.'%')
                ->with(['people'=>function ($q) use ($request){
                    $q->where('person_name','like','%'.$request->customer_name_search.'%');
                }])
                ->paginate('30');
        }
        elseif (isset($request->customer_name_search)){
            $preFactors = $preFactors
                ->with(['people'=>function ($q) use ($request){
                    $q->where('person_name','like','%'.$request->customer_name_search.'%');
                }])
                ->paginate('30');
        }
        elseif (isset($request->from_date)){
                if (isset($request->to_date)) {
                    $preFactors = $preFactors
                        ->where('pre_factor_date', '>=', $request->from_date)
                        ->where('pre_factor_date', '<=', $request->to_date)
                        ->paginate('30');
                }
                else{
                    $preFactors = $preFactors
                        ->where('pre_factor_date', '>=', $request->from_date)
                        ->paginate('30');
                }
        }
        elseif (isset($request->from_amount)){
            $preFactors = $preFactors
                ->where('amount','>=',str_replace(',','',$request->from_amount))
                ->where('amount','<=',str_replace(',','',$request->to_amount))
                ->paginate('30');
        }
        else{
            $preFactors = $preFactors->paginate('15');
        }
        if ($preFactors->isEmpty()){
            $tables .= '
             <tr>
                <th colspan="10"><p class="text-center">'.trans('validation.custom.notFound') .'</p></th>
            </tr>
            ';
        }
        else {
            $user = \Auth::getUser();
            foreach ($preFactors as $key => $preFactor) {

                if ($user->can('businessCrmProductDelete_not_view')){
                    $btnDelete='<button class="btn btn-link btn-text-danger btn-delete " data-name="'.$preFactor->name.'" 
                    data-id="'.$preFactor->slug.'" data-toggle="modal" data-target="#modal-delete" ><i class="ti-trash"></i></button>';
                }
                else{
                    $btnDelete='';
                }
                if ($user->can('businessCrmProductEdit')){
                    $btnEdit='<a class="btn btn-link btn-text-primary btn_edit" href="'.route('prefactors.edit',['slug'=>$preFactor->slug]).'"
                data-toggle="modal" data-target="#modal_edit" ><i class="ti-pencil-alt"></i></a>';
                }
                else{
                    $btnEdit='';
                }
                if ($user->can('businessCrmPreFactorShow')){
                    $btnDetail='<button class="btn btn-link text-success btn_details" data-slug="'.$preFactor->slug.'" 
                    data-toggle="modal" data-target="#modal_detail" ><i class="fa fa-info"></i></button>';
                }
                else{
                    $btnDetail='';
                }
                $btnPrint = '<a class="btn btn-link text-navy-blue btn_print" 
                        href="'.route('prefactors.after.insert',['slug'=>$preFactor->slug]).'">
                        <i class="fa fa-print"></i></a>';

                if($preFactor->position == 'preFactor') {
                    $position = '<label class="label label-info" > '.trans('validation.custom.crm.preFactors') .' </label >';
                }elseif($preFactor->position == 'convertFactor') {
                    $position ='<label class="label label-Purple" > '.trans('validation.custom.crm.convertFactors') .' </label >';
                }else {
                  $position =  '--';
                 }

                $row = $key + 1;
                $tables .='<tr>' .
                    '<td>' . $row . '</td>' .
                    '<td>' . $preFactor->pre_factor_code . '</td>' .
                    '<td>' . $preFactor->people->person_name . '</td>' .
                    '<td>' . $preFactor->pre_factor_date . '</td>' .
                    '<td>' . number_format($preFactor->amount + $preFactor->discount - $preFactor->preFactorDetail()->get()->sum('tax') ) . '</td>' .
                    '<td>' . number_format($preFactor->discount) . '</td>' .
                    '<td>' . number_format($preFactor->preFactorDetail()->get()->sum('tax')) . '</td>' .
                    '<td>' . number_format($preFactor->amount) . '</td>' .
                    '<td>' . $position . '</td>' .
                    '<td>'.$btnDelete.''.$btnEdit.''.$btnDetail.''.$btnPrint.'
                     </td>' .
                    '</tr>';
            }
        }
        return $tables;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return string
     */
    public function index(Request $request)
    {
        $preFactors=PreFactor::latest()->whereBranchId(ToolController::branchHerself()->id);
        if ($request->ajax()){
            return $this->search($request,$preFactors);
        }
        $preFactors = $preFactors->paginate('15');
        return view('business.crm.preFactors.all',compact('preFactors'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $branch = ToolController::branchHerself();
        $preFactors = PreFactor::latest()->whereBranchId($branch->id)->latest();
        $autoCode = ToolController::autoCode($preFactors->get(),'pre_factor_code',$preFactors);
        return view('business.crm.preFactors.create',compact('autoCode'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request->tax)) {
            $request->validate([
                'pre_factor_code' => 'required|unique:pre_factors',
                'customer_id' => 'required',
                'pre_factor_date' => 'required',
                'discount' => 'required|regex:/^[0-9\.,]+$/',
                'product_id.*' => 'required',
                'count.*' => 'required|numeric',
                'amount.*' => 'required|regex:/^[0-9\.,]+$/',
                'tax.*' => 'required|regex:/^[0-9\.,]+$/',
            ]);
        }
        else{
            $request->validate([
                'pre_factor_code' => 'required|unique:pre_factors',
                'customer_id' => 'required',
                'pre_factor_date' => 'required',
                'discount' => 'required|regex:/^[0-9\.,]+$/',
                'product_id.*' => 'required',
                'count.*' => 'required|numeric',
                'amount.*' => 'required|regex:/^[0-9\.,]+$/',
            ]);
        }
        $total = [];
        foreach ($request->amount as $key=>$value){
            if (isset($request->tax[$key])) {
                $tax = str_replace(',','',$request->tax[$key]);
            }
            else{
                $tax = 0;
            }
                $total[] = (str_replace(',','',$value) * str_replace(',','',$request->count[$key])) + $tax;
        }
        $discount =str_replace(',','',$request->discount);
        $total = array_sum($total) - $discount ;

        $preFactor = new PreFactor();
        $preFactor->slug = \App\Http\Controllers\ToolController::slug_random('6');
        $preFactor->branch_id = ToolController::branchHerself()->id;
        $preFactor->user_id = \Auth::getUser()->id;
        $preFactor->person_id = $request->customer_id;
        $preFactor->pre_factor_code = $request->pre_factor_code;
        $preFactor->detail = $request->detail;
        $preFactor->pre_factor_date = $request->pre_factor_date;
        $preFactor->amount = $total;
        $preFactor->discount = $discount;
        $preFactor->position = 'preFactor';
        $preFactor->save();

        foreach ($request->amount as $key=>$value) {

            $preFactorDetail = new PreFactorDetail();
            $preFactorDetail->slug = \App\Http\Controllers\ToolController::slug_random('6');
            $preFactorDetail->pre_factor_id = $preFactor->id;
            $preFactorDetail->amount = str_replace(',','',$value);
            $preFactorDetail->count = $request->count[$key];
            if (isset($request->tax[$key])) {
                $preFactorDetail->tax = str_replace(',','',$request->tax[$key]);
            }
            $preFactorDetail->product_id = $request->product_id[$key];
            $preFactorDetail->save();
        }
            if (ToolController::typeFactor() == true) {
                return json_encode(['status' => '120', 'redirect' => route('prefactors.after.insert', ['slug' => $preFactor->slug])]);
            }
            else{
                flash(trans('validation.custom.success') , 'success');
                return json_encode(['status'=>100]);
            }

        }

    /**
     * Display the specified resource.
     *
     * @param  \App\PreFactor  $preFactor
     * @return \Illuminate\Http\Response
     */
    public function show($preFactor)
    {
        $preFactor = PreFactor::whereSlug($preFactor)->first();

        $preFactorDetails = PreFactorDetail::latest()->wherePreFactorId($preFactor->id)->paginate(5);
        return view('business.crm.preFactors.details',compact('preFactor','preFactorDetails'));

    }


    /**
     * @param $preFactor
     * @return mixed
     */
    public function afterInsert($preFactor)
    {
        $preFactor = PreFactor::whereSlug($preFactor)->first();
        $preFactorDetails=PreFactorDetail::wherePreFactorId($preFactor->id)->get();
        $totalAmount = [];
        foreach ($preFactorDetails as $preFactorDetail){
            $totalAmount[]=$preFactorDetail->amount * $preFactorDetail->count;
        }
        $totalAmount = array_sum($totalAmount); //مجموع قیمت و مقدار بدون مالیات


        return view('business.crm.preFactors.afterInsert',compact('preFactor','preFactorDetails','totalAmount'));
    }

    public function convertPreFactor($preFactor)
    {
        $preFactor = PreFactor::whereSlug($preFactor)->first();

        if ($preFactor->position == "preFactor") {
            $preFactorDetails = $preFactor->preFactorDetail;


            $factors = Factor::whereBranchId(ToolController::branchHerself()->id)->latest();
            $autoCode = ToolController::autoCode($factors->get(), 'factor_code', $factors);
            // convert to factor
            $factor = new Factor();
            $factor->slug = \App\Http\Controllers\ToolController::slug_random(5);
            $factor->branch_id = ToolController::branchHerself()->id;
            $factor->user_id = \Auth::getUser()->id;
            $factor->person_id = $preFactor->person_id;
            $factor->factor_code = $autoCode;
            $factor->detail = "پیش فاکتور به فاکتور تبدیل شده است";
            $factor->type = 'saleInvoice';
            $factor->factor_date = Jalalian::now()->format('Y/m/d');
            $factor->amount = $preFactor->amount;
            $factor->discount = $preFactor->discount;
            $factor->save();

            $depot = Depot::whereBranchId(ToolController::branchHerself()->id)->whereDepotCode(2)->first(); //انبار فاکتور فروش خروج

            $sumAmount=[];

            foreach ($preFactorDetails as $preFactorDetail) {
                $factorDetail = new FactorDetail();
                $factorDetail->slug = \App\Http\Controllers\ToolController::slug_random(5);
                $factorDetail->factor_id = $factor->id;
                $factorDetail->product_id = $preFactorDetail->product_id;
                $factorDetail->count = $preFactorDetail->count;
                $factorDetail->amount = $preFactorDetail->amount;
                $factorDetail->tax = $preFactorDetail->tax;
                $factorDetail->save();

                //کسر از انبار
                $depotDetail = new DepotDetail();
                $depotDetail->product_id = $preFactorDetail->product_id;
                $depotDetail->factor_id = $factor->id;
                $depotDetail->slug = \App\Http\Controllers\ToolController::slug_random(4);
                $depotDetail->depot_id = $depot->id;
                $depotDetail->amount = $preFactorDetail->amount;
                $depotDetail->count = "-" . $preFactorDetail->count;
                $depotDetail->save();

                $sumAmount[]= $preFactorDetail->amount * $preFactorDetail->count;
            }
            $depot->count = $depot->count - array_sum($preFactorDetails->pluck('count')->toArray());
            $depot->amount = $depot->amount - array_sum($sumAmount);
            $depot->update();

            $preFactor->position = 'convertFactor';
            $preFactor->update();
            ToolController::factorAccountSave($factor,"پیش فاکتور به فاکتور تبدیل شده است");

            flash(trans('validation.custom.success'), 'success');
            return redirect(route('prefactors.index'));
        }
        else{
            flash(trans('validation.custom.validatePreFactor'), 'danger');
            return redirect(route('prefactors.index'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PreFactor  $preFactor
     * @return \Illuminate\Http\Response
     */
    public function edit($preFactor)
    {
        $preFactor = PreFactor::whereSlug($preFactor)->first();
        $preFactorDetails=PreFactorDetail::wherePreFactorId($preFactor->id)->get();
        return view('business.crm.preFactors.edit',compact('preFactor','preFactorDetails'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PreFactor  $preFactor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$preFactor)
    {
        if (isset($request->tax)) {
            $request->validate([
                'customer_id' => 'required',
                'pre_factor_date' => 'required',
                'discount' => 'required|regex:/^[0-9\.,]+$/',
                'product_id.*' => 'required',
                'count.*' => 'required|numeric',
                'amount.*' => 'required|regex:/^[0-9\.,]+$/',
                'tax.*' => 'required|regex:/^[0-9\.,]+$/',
            ]);
        }
        else{
            $request->validate([
                'customer_id' => 'required',
                'pre_factor_date' => 'required',
                'discount' => 'required|regex:/^[0-9\.,]+$/',
                'product_id.*' => 'required',
                'count.*' => 'required|numeric',
                'amount.*' => 'required|regex:/^[0-9\.,]+$/',
            ]);
        }
        $total = [];
        foreach ($request->amount as $key=>$value){
            if (isset($request->tax[$key])) {
                $tax = str_replace(',','',$request->tax[$key]);
            }
            else{
                $tax = 0;
            }
            $total[] = (str_replace(',','',$value) * str_replace(',','',$request->count[$key])) + $tax;
        }
        $discount =str_replace(',','',$request->discount);
        $total = array_sum($total) - $discount ;

        $preFactor = PreFactor::whereSlug($preFactor)->first();
        $preFactor->person_id = $request->customer_id;
        $preFactor->detail = $request->detail;
        $preFactor->pre_factor_date = $request->pre_factor_date;
        $preFactor->amount = $total;
        $preFactor->discount = $discount;
        $preFactor->update();

        $preFactorDetails = PreFactorDetail::wherePreFactorId($preFactor->id)->get();
        foreach ($preFactorDetails as $preFactorDetail){
            $preFactorDetail->delete();
        }

        foreach ($request->amount as $key=>$value) {
            $preFactorDetail = new PreFactorDetail();
            $preFactorDetail->slug = \App\Http\Controllers\ToolController::slug_random('6');
            $preFactorDetail->pre_factor_id = $preFactor->id;
            $preFactorDetail->amount = str_replace(',','',$value);
            $preFactorDetail->count = $request->count[$key];
            if (isset($request->tax[$key])) {
                $preFactorDetail->tax = str_replace(',','',$request->tax[$key]);
            }
            $preFactorDetail->product_id = $request->product_id[$key];
            $preFactorDetail->save();
        }

            flash(trans('validation.custom.edited') , 'success');
            return json_encode(['status'=>100]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PreFactor  $preFactor
     * @return \Illuminate\Http\Response
     */
    public function destroy(PreFactor $preFactor)
    {
        //
    }
}
